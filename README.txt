﻿Ubercart IDPay
=========================================
INSTALL:
=========================================
Install the module like every other drupal module
https://www.drupal.org/node/120641

=========================================
CONFIGURATION:
=========================================
the module configuration page:
admin/store/settings/payment/method/idpay

You should get your API Key from IDPay website from link:
https://idpay.ir/dashboard/web-services

The Sandbox option is for using the gateway in test environment.

=========================================
CREDITS
=========================================
https://idpay.ir/

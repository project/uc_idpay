<?php

/**
 * Implements hook_menu()
 */
function uc_idpay_menu() {
  $items['cart/idpay/complete'] = array(
    'title' => t('Payment Complete'),
    'page callback' => 'uc_idpay_complete',
    'access callback' => 'uc_idpay_access',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Access callback for cart/idpay/complete
 */
function uc_idpay_access() {
  return TRUE;
}

/**
 * Implements hook_uc_payment_gateway().
 */
function uc_idpay_uc_payment_gateway() {
  $gateways['uc_idpay'] = array(
    'title' => t('idpay.ir Gateway'),
    'description' => t('Process payments through idpay.ir gateway'),
  );
  return $gateways;
}

/**
 * Implements hook_uc_payment_method().
 */
function uc_idpay_uc_payment_method() {
  $title_logo = theme('image', array(
    'path' => drupal_get_path('module', 'uc_idpay') . '/assets/idpay.svg',
    'attributes' => array(
      'class' => array('uc-credit-cctype'),
      'style' => 'display: inline-block;max-width: 100px;'
    ),
  ));

  $methods = array();
  $methods['idpay'] = array(
    'id' => 'idpay_wps',
    'name' => t('idpay.ir online payment'),
    'title' => $title_logo . t('IDPay online payment'),
    'desc' => t('Redirect to idpay.ir Gateway to process payment.'),
    'callback' => 'uc_payment_method_idpay',
    'redirect' => 'uc_idpay_form',
    'weight' => 4,
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );

  return $methods;
}

/**
 * Payment method callback
 */
function uc_payment_method_idpay($op, &$order) {
  switch ($op) {
    case 'settings':
      $form['uc_idpay_api_key'] = array(
        '#type' => 'textfield',
        '#title' => t('idpay.ir API Key'),
        '#default_value' => variable_get('uc_idpay_api_key', ''),
        '#size' => 64,
      );
      $form['uc_idpay_sandbox'] = array(
        '#type' => 'checkbox',
        '#title' => t('Sandbox'),
        '#default_value' => variable_get('uc_idpay_sandbox', TRUE),
        '#required' => FALSE,
      );
      $form['uc_idpay_currency'] = array(
        '#type' => 'select',
        '#description' => t('Choose the currency type of your Ubercart store products'),
        '#title' => t('Currency Type'),
        '#default_value' => variable_get('uc_idpay_currency', 'toman'),
        '#options' => array('toman' => t('toman'), 'rial' => t('rial')),
        '#required' => TRUE,
      );

      $form['uc_idpay_success_message'] = array(
        '#type' => 'textarea',
        '#title' => t('idpay.ir success message'),
        '#default_value' => variable_get('uc_idpay_success_message', t('Your payment has been successfully completed. Tracking code: {track_id}')),
        '#description' => t("Enter the message you want to display to the customer after a failure occurred in a payment. You can also choose these placeholders {track_id}, {order_id} for showing the order id and the tracking id respectively.")
      );
      $form['uc_idpay_failed_message'] = array(
        '#type' => 'textarea',
        '#title' => t('idpay.ir failed message'),
        '#default_value' => variable_get('uc_idpay_failed_message', t('Your payment has failed. Please try again or contact the site administrator in case of a problem.')),
        '#description' => t("Enter the message you want to display to the customer after a failure occurred in a payment. You can also choose these placeholders {track_id}, {order_id} for showing the order id and the tracking id respectively.")
      );

      return $form;
      break;
    case 'cart-process':
      $_SESSION['pay_method'] = 'idpay';
      return NULL;
      break;
  }
}

/**
 * The payment form which will be submitted to the gateway.
 */
function uc_idpay_form($form, &$form_state, $order) {

  $user = user_load($order->uid);
  $amount = variable_get('uc_idpay_currency', 'toman') == 'toman' ? ($order->order_total * 10) : $order->order_total;
  $callback = url('cart/idpay/complete', array('absolute' => TRUE));
  $phone = $user->field_text_mobile['und'][0]? $user->field_text_mobile['und'][0]['value'] : '';
  $desc = t('payment for order number %s', array('%s' => $order->order_id));

  $result = idpay_send($order->order_id, $amount, $callback, $user->name, $phone, $user->mail, $desc);

  if (isset($result->id)) {
    $form['#method'] = 'get';
    $form['#action'] = $result->link;

    $form['submit'] = array(
      '#type'  => 'submit',
      '#value' => t('Submit Order')
    );

    return $form;
  }

  drupal_set_message(t('Error : @error', array('@error' => $result->error_message)), 'error');
  return;
}

/**
 * Menu callback for cart/idpay/complete
 */
function uc_idpay_complete() {
  $status   = !empty($_POST['status'])  ? $_POST['status']   : (!empty($_GET['status'])  ? $_GET['status']   : NULL);
  $track_id = !empty($_POST['track_id'])? $_POST['track_id'] : (!empty($_GET['track_id'])? $_GET['track_id'] : NULL);
  $id       = !empty($_POST['id'])      ? $_POST['id']       : (!empty($_GET['id'])      ? $_GET['id']       : NULL);
  $order_id = !empty($_POST['order_id'])? $_POST['order_id'] : (!empty($_GET['order_id'])? $_GET['order_id'] : NULL);

  $params = !empty($_POST['id']) ? $_POST : $_GET;

  if (empty($id) || empty($order_id)) {
    watchdog('idpay', 'Empty callback params.', array(),WATCHDOG_ERROR);

    drupal_goto('/cart/checkout');
    return '';
  }

  if ($status != 10) {
    $error = t('Error : @error', array('@error' => uc_idpay_get_status($status)));
    watchdog('idpay', $error . " <br><pre>". print_r($params, true) ."<pre>", array(), WATCHDOG_ERROR);

    $error .= "<br>". uc_idpay_filled_message('failed_message', $track_id, $order_id);
    $output = '<div class="text-center"><i class="mdi mdi-information-outline" style=" font-size: 100px; color: #FF5722;"></i>';
    $output .= '<p>'. $error .'</p><p><a href="/cart/checkout" class="btn btn-default">'. t('back') .'</a></p></div>';

    return $output;
  }

  $result = idpay_verify($id, $order_id);

  if (!isset($result->status)) {
    drupal_set_message($result->error_message, 'warning');
    drupal_goto('/cart/checkout');
    return $result->error_message;
  }

  if ($result->status != 100) {
    $error = uc_idpay_get_status($result->status);
    drupal_set_message($error, 'error');

    drupal_goto('/cart/checkout');
    return $error;
  }

  $order = uc_order_load($result->order_id);
  uc_payment_enter(
    $order->order_id,
    t('idpay.ir online payment'),
    $order->order_total,
    $order->uid,
    NULL,
    "payment successful<br><pre>". print_r($result, true) ."</pre>"
  );

  $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
  $message = uc_idpay_filled_message('success_message', $result->track_id, $result->order_id);

  watchdog('idpay', 'payment #%track_id verification succeeded', array('%track_id' => $result->track_id), WATCHDOG_INFO);

  $page = variable_get('uc_cart_checkout_complete_page', '');
  if (!empty($page)) {
    drupal_set_message($message);
    drupal_goto($page);
  }

  return '<p>' . $message . '</p>' . drupal_render($output);
}

/**
 * @param      $order_id
 * @param      $amount
 * @param      $callback
 * @param null $name
 * @param null $phone
 * @param null $mail
 * @param null $desc
 *
 * @return bool|string
 */
function idpay_send($order_id, $amount, $callback, $name = null, $phone = null, $mail = null, $desc = null) {
  $api_key = variable_get('uc_idpay_api_key', '');
  $sandbox = variable_get('uc_idpay_sandbox', '');

  if (empty($api_key)) {
    $msg = t('idpay.ir gateway is not configured.');
    drupal_set_message($msg, 'error');
    drupal_goto('/cart/checkout');
    return $msg;
  }

  $header = array(
    'Content-Type: application/json',
    "X-API-KEY: {$api_key}",
    "X-SANDBOX: {$sandbox}"
  );
  $body = array(
    'order_id' => $order_id,
    'amount' => $amount,
    'name' => $name,
    'phone' => $phone,
    'mail' => $mail,
    'desc' => $desc,
    'callback' => $callback,
  );

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://api.idpay.ir/v1.1/payment');
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

  $result = curl_exec($ch);
  curl_close($ch);

  $result = json_decode($result);

  if (!isset($result->id)) {
    watchdog('idpay', "payment creation failed. <br><pre>". print_r($body, true) ."<pre>", array(),WATCHDOG_ERROR);
  }

  return $result;
}

/**
 * verify payment
 *
 * @param $id
 * @param $order_id
 *
 * @return bool|string
 */
function idpay_verify($id, $order_id) {
  $api_key = variable_get('uc_idpay_api_key', '');
  $sandbox = variable_get('uc_idpay_sandbox', '');

  $header = array(
    'Content-Type: application/json',
    "X-API-KEY: {$api_key}",
    "X-SANDBOX: {$sandbox}"
  );
  $body = array(
    'id' => $id,
    'order_id' => $order_id,
  );

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://api.idpay.ir/v1.1/payment/verify');
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

  $result = curl_exec($ch);
  curl_close($ch);

  $result = json_decode($result);

  if (!isset($result->status)) {
    watchdog('idpay', "payment verifying failed. <br><pre>". print_r($body, true) ."<pre>", array(),WATCHDOG_ERROR);
  }

  return $result;
}

/**
 * @param $type
 * @param $track_id
 * @param $order_id
 *
 * @return string|string[]|null
 */
function uc_idpay_filled_message($type, $track_id, $order_id) {
  $message = variable_get("uc_idpay_{$type}");
  return str_replace( [ "{track_id}", "{order_id}" ], [
    $track_id,
    $order_id,
  ], $message );
}

/**
 * @param $status_code
 *
 * @return string
 */
function uc_idpay_get_status($status_code) {
  switch ($status_code) {
    case 1:
      return 'پرداخت انجام نشده است';
      break;
    case 2:
      return 'پرداخت ناموفق بوده است';
      break;
    case 3:
      return 'خطا رخ داده است';
      break;
    case 4:
      return 'بلوکه شده';
      break;
    case 5:
      return 'برگشت به پرداخت کننده';
      break;
    case 6:
      return 'برگشت خورده سیستمی';
      break;
    case 7:
      return 'انصراف از پرداخت';
      break;
    case 8:
      return 'به درگاه پرداخت منتقل شد';
      break;
    case 10:
      return 'در انتظار تایید پرداخت';
      break;
    case 100:
      return 'پرداخت تایید شده است';
      break;
    case 101:
      return 'پرداخت قبلا تایید شده است';
      break;
    case 200:
      return 'به دریافت کننده واریز شد';
      break;
    default :
      return 'خطای ناشناخته';
      break;
  }
}
